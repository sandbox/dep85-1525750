<?php
/*
 *@file
 * callbacks for circles
 */

/**
 * Implements add_circle callback
 */
function add_circles() {

  $form['add_circle'] = array(
    '#type' => 'textfield',
    '#title' => t('New circle'),
    '#description' => t('Add new circle of users'),
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add circle'),
  );

  return $form;
}

function add_circles_submit($form, $form_state) {
  global $user;

  $circle_name = check_plain($form_state['input']['add_circle']);

  $query = db_insert('circles')->fields(array(
      'uid' => $user->uid,
      'name' => $circle_name,
      'acl_id' => 0,
    ))->execute();


  $last_cid = db_query('SELECT c.cid FROM {circles} as c where c.uid = :uid ORDER BY c.cid DESC', array(':uid' => $user->uid))->fetchField();
  $acl_name = 'cid' . $last_cid;
  //create acl
  $acl_id = acl_create_new_acl('user_relationship_circles', $acl_name);
  //add author to own circle
  acl_add_user($acl_id, $user->uid);

  //update acl_id
  $query = db_update('circles')->fields(array(
      'acl_id' => $acl_id,
    ))->condition('cid', $last_cid, '=')->execute();

  drupal_set_message(t('Circle %name has been added.'), array('%name' => $circle_name));
}

/**
 * Implements user_circles
 */
function user_circles($uid) {
  $query = db_select('circles', 'c');
  $query->condition('uid', (int)$uid)->fields('c', array('cid', 'name'));

  $results = $query->execute();

  $circles_list = array();
  $i = 0;
  foreach ($results as $result) {
    $acl_name = 'cid' . $result->cid;
    $acl_id = acl_get_id_by_name('user_relationship_circles', $acl_name);
    //subtract 1 beacuse author belong to own circle
    $counter = acl_has_users($acl_id) - 1;

    $circles_list[$i][] = l("$result->name ($counter)", "user/$uid/circles/$result->cid", array());
    $circles_list[$i][] = l(t('edit'), "user/$uid/circles/$result->cid/edit", array('attributes' => array('class' => '', 'title' => 'Edit')));
    $circles_list[$i][] = l(t('delete'), "user/$uid/circles/$result->cid/delete", array('attributes' => array('class' => '', 'title' => 'Delete')));
    $i++;
  }

  $add_new_circle = l(t('Add new circle'), 'circles/add', array('attributes' => array('class' => '', 'title' => 'Add new circle')));

  $headers = array(t('Circle name'), t('Edit'), t('Delete'));
  return theme('table', array('header' => $headers, 'rows' => $circles_list, 'attributes' => array('class' => 'table-item'), 'sticky' => FALSE)) . $add_new_circle;
}

/**
 * Implements user_circles_view
 */
function user_circles_view($uid, $cid) {
  $uid = (int)$uid;
  $cid = (int)$cid;

  $acl_name = 'cid' . $cid;

  $acl_id = acl_get_id_by_name('user_relationship_circles', $acl_name);
  $acl_users = acl_get_uids($acl_id);

  $user_list = array();
  foreach ($acl_users as $acl_user) {
    // hide author of circle from list
    if ($acl_user != $uid) {
      $user_account = user_load($acl_user);
      $user_list[] = theme('user_picture', array('account' => $user_account)) . theme('username', array('account' => $user_account));
    }
  }

  $list = theme('item_list', array('items' => $user_list, 'attributes' => array('class' => array(''))));
  $links = l(t('Edit'), "user/$uid/circles/$cid/edit", array('arguments' => array('class' => '', 'title' => 'Edit circle')));

  return $list . $links;
}

function user_circles_view_title($cid) {
  $name = _get_circle_name($cid);
  drupal_set_title(t('Circle: @title', array('@title' => $name)));
}

/**
 * Implements user_circles_edit
 */
function user_circles_edit_title($cid) {
  $cid = (int)$cid;
  $name = _get_circle_name($cid);
  drupal_set_title(t('Edit circle: @name', array('@name' => $name)));
}

function user_circles_edit($form, &$form_state, $uid, $cid) {
  $uid = (int)$uid;
  $cid = (int)$cid;

  $relationships_types = variable_get('user_relationship_circles_relationships_types');
  $users = user_relationships_load($param = array("rtid" => array(1), "approved" => $relationships_types, "user" => $uid), $options = array(), $reset = FALSE);

  foreach ($users as $user) {
    $user_account = user_load($user->requestee_id);
    $options[$user->requestee_id] = theme('username', array('account' => $user_account));
  }

  if ($options) {
    $form['choose_users_' . $cid] = array(
      '#type' => 'checkboxes',
      '#title' => t('Choose a users.'),
      '#description' => 'Choose users to circle.',
      '#options' => $options,
      '#default_value' => variable_get('choose_users_' . $cid, array()),
    );

    $form['cid'] = array(
      '#type' => 'value',
      '#value' => $cid,
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
    );
  }
  else {
    $form['#prefix'] = t('No users to choose');
  }

  return $form;
}

function user_circles_edit_submit($form, &$form_state) {

  $cid = $form_state['values']['cid'];

  $acl_name = 'cid' . $cid;
  $acl_id = acl_get_id_by_name('user_relationship_circles', $acl_name);

  foreach ($form_state['input']['choose_users_' . $cid] as $key => $value) {
    //if default value is not isset (first save)
    if (!$form_state['complete form']['choose_users_' . $cid]['#default_value']) {
      if ($value) {
        //add user to acl
        acl_add_user($acl_id, $value);
      }
      else {
        //remove user from acl
        //acl_remove_user($acl_id, $form_state['complete form']['choose_users_'.$cid]['#default_value'][$key]);
      }
    }
    elseif ($form_state['complete form']['choose_users_' . $cid]['#default_value'][$key] != $value) {
      if ($value) {
        acl_add_user($acl_id, $value);
      }
      else {
        acl_remove_user($acl_id, $form_state['complete form']['choose_users_' . $cid]['#default_value'][$key]);
      }
    }
  }

  variable_set('choose_users_' . $cid, $form_state['input']['choose_users_' . $cid]);
  drupal_set_message(t('Changes has been saved.'));
}

/**
 * Implements user_circles_delete
 */
function user_circles_delete($form, &$form_state, $uid, $cid) {
  global $user;
  $cid  = (int)$cid;
  $uid  = (int)$uid;
  $name = _get_circle_name($cid);

  if ($cid && $name) {

    $form['cid'] = array(
      '#type' => 'value',
      '#value' => $cid,
    );

    $form['uid'] = array(
      '#type' => 'value',
      '#value' => $uid,
    );

    return confirm_form(
      $form,
      t('Are you sure you want to delete circle %name?', array('%name' => $name)),
      "user/$uid/circles",
      t('This action cannot be undone.'),
      t('Delete'), t('Cancel')
    );
  }
  else {
    drupal_set_message(t("Circle doesn't exist"), 'warning');
    drupal_goto("user");
  }
}

function user_circles_delete_submit($form, &$form_state) {

  $cid  = $form_state['values']['cid'];
  $uid  = $form_state['values']['uid'];
  $name = _get_circle_name($cid);

  $acl_name = 'cid' . $cid;
  $acl_id = acl_get_id_by_name('user_relationship_circles', $acl_name);

  //take all nodes associated with $acl_id
  $query = db_select('acl_node', 'an');
  $query->condition('acl_id', (int)$acl_id)->fields('an', array('nid'));
  $results = $query->execute();

  //remove acl
  acl_delete_acl($acl_id);

  //change all premmisions from associated node
  foreach ($results as $node_id) {
    $node = node_load($node_id->nid);
    node_save($node);

    $associate_acl = db_query("SELECT COUNT(acl_id) FROM {acl_node} where nid = :nid", array(':nid' => $node_id->nid))->fetchField();
    if (!$associate_acl) {
      db_delete('circles_access')->condition('nid', $node_id->nid)->execute();
    }
  }

  //remove
  $query = db_delete('circles')->condition('cid', $cid)->execute();

  drupal_set_message(t('Circle %name has been deleted.', array('%name' => $name)));
  watchdog(
    'user_relationship_circles',
    'Circle %name has been deleted', array('%name' => $name),
    WATCHDOG_NOTICE
  );

  $form_state['redirect'] = "user/$uid/circles";
}

/**
 * Implements _get_circle_name
 */
function _get_circle_name($cid) {
  $query = db_query('SELECT c.name FROM {circles} as c where c.cid = :cid', array(':cid' => (int)$cid))->fetchField();
  return $query;
}

